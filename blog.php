<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Miriam Marco, estudio de fotografía</title>
    <meta name="description" content="estudio de fotografía, en Teruel">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="row">
	<div class="col-md-12" id="logo">
			<img src="imagenes/logo.jpg" class="center-block">	
	</div>
</div>
<div class="row">
	<div class="col-md-12" id="menu">
		<ul>
			<li><a href="index.php"  id="home">HOME</a></li>
			<li><a href="bodas.php" id="bodas">BODAS</a></li>
			<li><a href="contacto.php" id="contacto">CONTACTO</a></li>
			<li><a href="blog.php" class="activo" id="blog">BLOG</a></li>
			<li><a href="login.php" id="private">AREA PRIVADA</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10 col-xs-12 col-sm-12" id="blog">
			<!--<iframe src="http://www.miriammarco.com/"></iframe>-->
			<!--<script src="http://feeds.feedburner.com/miriammarco/Zwon?format=sigpro" type="text/javascript" ></script>-->
	</div>
	<div class="col-md-1"></div>
</div>

<div class="row">
	<div class="col-md-5"></div>
	<div class="col-md-2 col-xs-12 col-sm-12" id="footer-social">
		<div class="container"><?php include("footer.html");?></div>
	</div>
	<div class="col-md-5"></div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript">
	$("#menu a").click(function(){
		var id = $(this).attr("id");
		$(".activo").removeClass("activo");
		$("#" + id).addClass("activo");
	})
	
	for(i=0;i<5;i++){
		$("#blog").append("<div class='cont_blog'></div>")
	}
</script>
</body>
</html>