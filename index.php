<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Miriam Marco, estudio de fotografía</title>
    <meta name="description" content="estudio de fotografía, en Teruel">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    
    <link rel="stylesheet" type="text/css" href="css/styles.css">	
 
    <style type="text/css">
    .lightbox-gallery div{
    	float: left;
    	margin: 10px;

    }
    	.lightbox-gallery img{
    		width: 200px;
    	}
    	.oculto{
    		display: none;
    	}
    	.fotorama{
    		background-color: #222;
    		position: relative;
    	}
    	.i{
			margin-left: 25px;
    	}
    	.d{
    		margin:auto; 	
    	}
    	#imagen_grande{
    		
    		margin: auto;
    		width: 85%;
    		position: relative;
    		
    	}
    	#fila_generada button{
    		background-color: #454545;
    		padding-top:20px;
    		padding-bottom: 20px;
    		font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;

    		border: 1px solid #141414;
    		border-radius: 2px;
    		font-size: 30px;
    		padding-top: 30px;
    		padding-bottom: 30px;
    		padding-left: 8px;
    		padding-right: 8px;
    		color: #aaa !important;
    		cursor: pointer !important;
    		display: inline-block;
    		font-weight: 2000;
		    margin: 0 5px;
		    outline: 0;
		    background-image: -webkit-linear-gradient(top,#3e3e3e,#333);
		    text-align: center;
    		box-shadow: 0 1px 0 rgba(255,255,255,.06), 1px 1px 0 rgba(255,255,255,.03), -1px -1px 0 rgba(0,0,0,.02), inset 1px 1px 0 rgba(255,255,255,.05);	
    		top: 40%;	    
    	}
    	#fila_generada button:hover{
    		
    		background-color: #fff;
    		color: #222;
    	}

    </style>
</head>
<body>

<div class="row">
	<div class="col-md-12" id="logo">
			<img src="imagenes/logo.jpg" class="center-block">	
	</div>
</div>
<div class="row">
	<div class="col-md-12" id="menu">
		<ul>
			<li><a href="index.php" class="activo" id="home">HOME</a></li>
			<li><a href="bodas.php" id="bodas">BODAS</a></li>
			<li><a href="contacto.php" id="contacto">CONTACTO</a></li>
			<li><a href="blog.php" id="blog">BLOG</a></li>
			<li><a href="login.php" id="private">AREA PRIVADA</a></li>
		</ul>
	</div>
</div>
<!--<div class='row'>
	<div class='col-md-2'></div>
	<div class='col-md-8 center-block' id='fotorama'>

		<button class='btn btn-outline-secondary' id='izq'><<</button>
		<img id='imagen_grande'>
		<button class='btn btn-outline-secondary' id='dcha'>>></button>
		
	</div>
	<div class='col-md-2'></div>
</div>-->
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10 container">
		<div class="lightbox-gallery">
			<div class="row">
				
				<div class="col-md-2"><img src="imagenes/fotos/Foto-1.jpg" id="1"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-2.jpg" id="2"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-3.jpg" id="3"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-4.jpg" id="4"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-5.jpg" id="5"></div>
				
			</div>
			<div class="row">
				
				<div class="col-md-2"><img src="imagenes/fotos/Foto-6.jpg" id="6"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-7.jpg" id="7"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-8.jpg" id="8"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-9.jpg" id="9"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-10.jpg" id="10"></div>
				
			</div>
			<div class="row">
				<div class="col-md-2"><img src="imagenes/fotos/Foto-11.jpg" id="11"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-12.jpg" id="12"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-13.jpg" id="13"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-14.jpg" id="14"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-15.jpg" id="15"></div>
			</div>
			<div class="row">
				<div class="col-md-2"><img src="imagenes/fotos/Foto-16.jpg" id="16"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-17.jpg" id="17"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-18.jpg" id="18"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-19.jpg" id="19"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-20.jpg" id="20"></div>
			</div>
			<div class="row">
				<div class="col-md-2"><img src="imagenes/fotos/Foto-21.jpg" id="21"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-22.jpg" id="22"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-23.jpg" id="23"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-24.jpg" id="24"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-25.jpg" id="25"></div>
			</div>
			<div class="row">
				<div class="col-md-2"><img src="imagenes/fotos/Foto-26.jpg" id="26"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-27.jpg" id="27"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-28.jpg" id="28"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-29.jpg" id="29"></div>
				<div class="col-md-2"><img src="imagenes/fotos/Foto-30.jpg" id="30"></div>
			</div>
			
		</div>
	</div>
	
	<div class="col-md-1"></div>
</div>

<div class="row">
	<div class="col-md-5"></div>
	<div class="col-md-2 col-xs-12 col-sm-12" id="footer-social">
		<div class="container"><?php include("footer.html");?></div>
	</div>
	<div class="col-md-5"></div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>


<script type="text/javascript">
	$("#menu a").click(function(){
		var id = $(this).attr("id");
		$(".activo").removeClass("activo");
		$("#" + id).addClass("activo");
	});
	$(".lightbox-gallery img").css("cursor","pointer");
	$(".lightbox-gallery img").click(function(){
		//$("#fotorama").removeClass("oculto");
		$(".fotorama").remove();
		var id_img = $(this).attr("id");
		var id_img_a_sumar = parseInt(id_img);
		var src_img = $(this).attr("src");
		var row = "<div id='fila_generada' class='row fotorama '><div id='celda_generada' colspan='5' class='col-md-12 col-xs-18 col-sm-12'><button class='i _GKB' id='izq'><b><</b></button><img id='imagen_grande'><button class='d _GKB' id='dcha'><b>></b></button></div></div>";
		
		$(this).parent().parent().after(row);
		
		$("#imagen_grande").attr("src",src_img);
			$(".d").click(function(){

				var id_next = id_img_a_sumar + 1;
				if(id_next>30){
					id_img_a_sumar = -1;
					
				}
				var src_next = $("#"+id_next).attr("src");
				$("#imagen_grande").attr("src",src_next);
				id_img_a_sumar = id_img_a_sumar + 1;
			
			});
			$(".i").click(function(){
				var id_next = id_img_a_sumar - 1;
				if(id_next<1){
					id_img_a_sumar = 32;
				}
				var src_next = $("#"+id_next).attr("src");
				$("#imagen_grande").attr("src",src_next);
				
				id_img_a_sumar = id_img_a_sumar - 1;
			});

	});

</script>

</body>
</html>