<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Miriam Marco, estudio de fotografía</title>
    <meta name="description" content="estudio de fotografía, en Teruel">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
        <style type="text/css">
      .err{
        display: none;
      }
    </style>
</head>
<body>
<div class="row">
	<div class="col-md-12" id="logo">
			<img src="imagenes/logo.jpg" class="center-block">	
	</div>
</div>
<div class="row">
	<div class="col-md-12" id="menu">
		<ul>
			<li><a href="index.php"  id="home">HOME</a></li>
			<li><a href="bodas.php" id="bodas">BODAS</a></li>
			<li><a href="contacto.php" class="activo" id="contacto">CONTACTO</a></li>
			<li><a href="blog.php"  id="blog">BLOG</a></li>
			<li><a href="login.php" id="private">AREA PRIVADA</a></li>
		</ul>
	</div>
</div>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-3 col-xs-6 col-sm-6" id="map">
		<h2><b>CONTACTO</b></h2>
		<h4><b>Miriam Marco Fotografía</b></h4>
		<p>info@miriammarcofotografia.com</p>
		<p>Tfno:686970477</p>
		<a target="_blank" href="https://www.google.es/maps/place/Calle+de+la+Parra,+38,+44001+Teruel/@40.3406307,-1.1057329,20.5z/data=!4m5!3m4!1s0xd5e73063de872df:0xf39ef1773a08a907!8m2!3d40.3406052!4d-1.1056575"><img src="imagenes/mapa.png" height="200" class="img-responsive"></a>
	</div>
	<div class="col-md-3 col-xs-6 col-sm-6" id="cont">
		<form id="formulario" method="POST" action="php/send.php">
		<div class="form-group" >
		    <label for="nombre">Nombre (required)</label>
		    <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre" required>
		</div>
		<div class="form-group">
		    <label for="email">Email (required)</label>
		    <input type="email" class="form-control" id="email"  placeholder="Email" name="mail">
		</div>
		<div class="form-group">
		    <label for="telefono">Telefono (required)</label>
		    <input type="text" class="form-control" id="telefono" placeholder="Telefono para contactar" name="tfno" required>
		</div>		
		<div class="form-group">
		    <label for="asunto">Asunto (required)</label>
		    <input type="text" class="form-control" id="asunto"  placeholder="Asunto" name="subject">
		</div>
		  <div class="form-group">
		    <label for="mensaje">Mensaje (required)</label>
		    <textarea class="form-control" id="mensaje" rows="4" name="mensaje"></textarea>
		  </div>
		  <div class="form-group">
		  	<input type="checkbox" name="captcha" id="captcha">
		  	<label for="captcha">No soy un robot</label>
		  </div>	
		  <div id="error_captcha" class="alert alert-danger err" role="alert"></div>	  
		  <div class="container">
		  <button type="button" class="btn btn-outline-secondary" id="enviar">Enviar</button>				
		  </div>

		</form>
	</div>
	<div class="col-md-3"></div>
</div>

<div class="row">
	<div class="col-md-5"></div>
	<div class="col-md-2 col-xs-12 col-sm-12" id="footer-social">
		<div class="container"><?php include("footer.html");?></div>
	</div>
	<div class="col-md-5"></div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript">
	$("#menu a").click(function(){
		var id = $(this).attr("id");
		$(".activo").removeClass("activo");
		$("#" + id).addClass("activo");
	});
	$("#enviar").click(function(){
		nombre = $("#nombre").val();
		mail = $("#email").val();
		tfno = $("#telefono").val();
		asunto = $("#asunto").val();
		mensaje = $("#mensaje").val();
		if($("#captcha").is(":checked")){
			if(nombre.length < 1 || mail.length <1 || tfno <1 || asunto<1 ||mensaje<1){
			$("#error_captcha").empty();
			$("#error_captcha").append("<p><b>Todos los campos son obligatorios</b></p>");
			$("#error_captcha").removeClass("err").css("display","block").fadeOut(6000);
			
			}else{
				$("#formulario").submit();
			}

		}else{
			$("#error_captcha").empty();
			$("#error_captcha").append("<p><b>Si eres un humano, selecciona la casilla</b></p>");
			$("#error_captcha").removeClass("err").css("display","block").fadeOut(6000);
		}
	});

</script>
</body>
</html>