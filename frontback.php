<?php
include("php/conexion.php");
    session_start();
    if(!isset($_SESSION['name']))
    {
       // header("location: ../index.php");
    }
    $name=$_SESSION['name'];
    $id_cliente = $_SESSION['id_cliente'];
    ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Miriam Marco, estudio de fotografía</title>
    <meta name="description" content="estudio de fotografía, en Teruel">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <style type="text/css">
    	.fotos{
			border:solid 2px #ddd;	
			border-radius: 5px;
		}
		.ok{
			background-image: url(imagenes/tick.png);
			background-repeat: no-repeat;
			opacity: 1;
			background-position: center;
		}
		.contenedor{
			float: left;
			margin-left:10px;
		}
		.foto_click{
			
			opacity: 0.5;
			border: solid 3px #ff0000;
			border-radius: 5px;
		}
		input[type=checkbox]{
			display:none;
		}
		#main img{

  -moz-transition: all 0.3s;
  -webkit-transition: all 0.3s;
  transition: all 0.3s;
		}
		#main img:hover{
			  -moz-transform: scale(3);
  -webkit-transform: scale(3);
  transform: scale(3);
		}
    </style>
</head>
<body>
<?php
include("php/funciones.php");
?>
<div class="row" id="header">
	<div class="col-md-2"></div>
	<div class="col-xs-6 col-md-2" id="logo">
			<img src="imagenes/logo.jpg">
	</div>
	<div class="col-xs-12 col-sm-6 col-md-8" id="menu">
		<ul>
			<li><a href="php/cerrar.php">Cerrar sesión</a></li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-md-12 col-xs-12 col-sm-12" id="main">
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12">
				<h1>Bienvenido <?php echo $name;?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12" id="sesiones_usuario">
			<div class="container">
			<form action="php/send_miriam.php" method="POST" id="enviar_fotos">
			
				<?php mostrar_sesion();?>
			
			
			
			<div class="row">
			<div class="col-md-12 col-xs-12 col-sm-12 " align="center">
			<br><button class="btn btn-success btn-lg" type="button" id="send">Enviar</button></div>
			</div>

			</form>
			</div>
			</div>
			</div>
		</div>

</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript">
	$("#main img").click(function(){
		var id = $(this).attr("alt");
			if($("#" + id).is(':checked')){
				$(this).removeClass("foto_click ");
				$("#contenedor"+id).removeClass("ok");
				$("#" + id).prop("checked", false);
			}else{
				$(this).addClass("foto_click");
				$("#contenedor"+id).addClass("ok");
				$("#" + id).prop("checked", true);
			}
	});
	$("#send").click(function(){
		$("#enviar_fotos").submit();
	})
</script>
</body>
</html>