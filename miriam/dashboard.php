<?php
include("inc/conexion.php");
    session_start();
    if(!isset($_SESSION['name']))
    {
       // header("location: ../index.php");
    }
    $name=$_SESSION['name'];
    $id_cliente = $_SESSION['id_cliente'];
    ?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Miriam Marco, estudio de fotografía</title>
    <meta name="description" content="estudio de fotografía, en Teruel">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <style type="text/css">
    	#delete_sesion{
	float: right;
}
	body{
		font-family: Arial;
	}
.loading{
    margin-left: 40%;
    margin-top: -10%;
}
    </style>   
<script>
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];

        }
    }

};
if (getUrlParameter('v')==1){


 $('#loading').hide();
}

</script>
</head>
<body>
<?php
include("inc/funciones.php");
?>
<div class="row" id="header">
	<div class="col-md-2"></div>
	<div class="col-xs-6 col-md-2" id="logo">
			<img src="../imagenes/logo.jpg">
	</div>
	<div class="col-xs-12 col-sm-6 col-md-8" id="menu">
		<ul>
			<li><a href="../php/cerrar.php">Cerrar Sesion</a></li>
		</ul>
	</div>
</div>
<div id="loading" style="display:none">
	<img src="images/icon-load.gif" class="loading">

</div>
<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-8 col-xs-12 col-sm-12" id="main">
			<div id="myTab">
			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Nuevo Cliente</a></li>
			    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Nueva Sesion</a></li>
			    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Asignar Fotos</a></li>
			  </ul>

			  <!-- Tab panes -->
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="home">
			    	<form action="inc/cliente.php" method="POST" id="alta_cliente">
			    		 <div class="form-group">
 							<label for="nombre">Nombre</label>
    						<input type="text" class="form-control" id="nombre" placeholder="Nombre Cliente" name="nombre">
  						</div>
			    		 <div class="form-group">
 							<label for="apellidos">Apellidos</label>
    						<input type="text" class="form-control" id="apellidos" placeholder="Apellidos Cliente" name="apellidos">
  						</div>
			    		 <div class="form-group">
 							<label for="dni">DNI</label>
    						<input type="text" class="form-control" id="dni" placeholder="DNI Cliente" name="dni">
  						</div> 
			    		 <div class="form-group">
 							<label for="usuario">Usuario</label>
    						<input type="text" class="form-control" id="usuario" placeholder="Nombre de usuario Cliente" name="usuario">
  						</div>
			    		 <div class="form-group">
 							<label for="contrasena">Contraseña</label>
    						<input type="text" class="form-control" id="contrasena" placeholder="Contraseña para usuario Cliente" name="contrasena">
  						</div>
  						<button type="button" class="btn btn-primary" id="send_client">Dar Alta</button>  						  						 						  					
			    	</form>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="profile">
			    	<form id="alta_sesion" action="inc/sesion.php" method="POST">
			    		<div class="form-group">
    						<label for="cliente">Selecciona Cliente</label>
    							<select class="form-control" id="cliente" name="cliente">
    									<?php select_clientes();?>
    							</select>
  						</div>
			    		 <div class="form-group">
 							<label for="sesion">Nombre</label>
    						<input type="text" class="form-control" id="sesion" placeholder="Nombre de la Sesion" name="sesion">
  						</div>  						
  						<button type="button" class="btn btn-primary" id="send_sesion">Guardar Sesion</button>
			    	</form>
			    </div>
			    <div role="tabpanel" class="tab-pane" id="messages">
			    	<form id="alta_fotos" action="inc/upload.php" method="POST" enctype="multipart/form-data">
			    		<div class="form-group">
    						<label for="cliente_foto">Selecciona Cliente</label>
    							<select class="form-control" id="cliente_foto" name="cliente_foto">
    								<option></option>
    								<?php select_clientes();?>
    							</select>
  						</div>
			    		<div class="form-group">
    						<label for="sesion_foto">Selecciona Sesion</label>
    							<select class="form-control" id="sesion_foto" name="sesion_foto">
    								
    							</select>
  						</div>
						  <div class="form-group">
						    <label for="fotos">Seleciona Fotos</label>
						    <input type="file" class="form-control" id="fotos" placeholder="Fotos" name="fotos[]" accept="image/*" multiple="multiple">
						  </div>
						<button type="button" class="btn btn-primary" id="send_fotos" name="send_fotos">Guardar Fotos</button>  				
						<button type="button" class="btn btn-danger" id="delete_sesion" name="delete_sesion">Borrar Sesion</button>		  								    		
			    	</form>
<div class="alert alert-success alert-dismissible" role="alert" style="display: none">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true" id="cerrar_alert">&times;</span></button>
  <strong>Sesión borrada exitosamente!</strong>
</div>			    	
			    </div>
			  </div>
			</div>
	</div>
	<div class="col-md-2"></div>
        
</div>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript">
$('#myTab li:eq(0) a').tab('show');
$('#myTab li:eq(1) a').tab('show');
$('#myTab li:eq(2) a').tab('show');
</script> 
<script type="text/javascript">
	$("#send_client").click(function(){
		$('#alta_cliente').submit();
	});
	$("#send_sesion").click(function(){
		$('#alta_sesion').submit();
	});
	$("#cliente_foto").change(function(){
		$("#sesion_foto option").remove();
		var id_cliente = $("#cliente_foto").val();
		$.getJSON( "inc/obtener_sesion.php",{id:id_cliente})
		.done(function(json){
			numero_sesiones = json.length;
				for (i=0 ; i < numero_sesiones ; i++){
					nombre_sesion = json[i].nombre_sesion;
					id_session = json[i].id_sesion;
					$("#sesion_foto").append("<option value=" + id_session + ">" + nombre_sesion + "</option>");
				};
		});
	});

	$("#send_fotos").click(function(){
        $("#loading").show();

		$("#alta_fotos").submit();


	});
	$("#delete_sesion").click(function(){
		id_client = $("#cliente_foto").val();
		nom_ses = $("#sesion_foto").text();
		id_ses = $("#sesion_foto").val();
		$.post( "../borrar_sesion.php",{idc:id_client,ids:id_ses,nom_ses:nom_ses});
		$(".alert-success").show();
	});
	$("#cerrar_alert").click(function(){
		location.reload();
	})
</script>
</body>
</html>