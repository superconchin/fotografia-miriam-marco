<?php
include("php/conexion.php");
session_start();
if (isset($_POST['usuario'])){ 
  $nom =$_POST['usuario'];
  $p = $_POST['password'];
  $sql = "SELECT * FROM clientes WHERE user='$nom' AND pass='$p'";
  $rs = mysql_query($sql);
  if($nom == "miriam.marco" && $p="teruel"){
    header("location:miriam/dashboard.php");
  }else{
  while($fila = mysql_fetch_row($rs)){
      if($nom == $fila[4]){
        if($p == $fila[5]){
          session_start();
          $_SESSION['name']=$_POST['usuario'];
          $_SESSION['id_cliente'] = $fila[0];
          header("location: frontback.php");
        }else{
          echo "La contraseña es incorrecta";
          //echo "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
          //<span aria-hidden='true'><h2>Error en login!</h2><p>La contraseña es incorrecta</p></span>
        //</button>";
        $mensaje = "<h2>Error en login!</h2><p>La contraseña es incorrecta</p>";         

        }
      }else{
        $mensaje = "<h2>Error en login!</h2><p>El usuario no está registrado.</p>";
        //echo "El usuario no es válido";
      }



    }
  }
  }
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Miriam Marco, estudio de fotografía</title>
    <meta name="description" content="estudio de fotografía, en Teruel">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <style type="text/css">
      .err{
        display: none;
      }
    </style>
</head>
<body>
<div class="row">
  <div class="col-md-12" id="logo">
      <img src="imagenes/logo.jpg" class="center-block">  
  </div>
</div>
<div class="row">
  <div class="col-md-12" id="menu">
    <ul>
      <li><a href="index.php"  id="home">HOME</a></li>
      <li><a href="bodas.php" id="bodas">BODAS</a></li>
      <li><a href="contacto.php" id="contacto">CONTACTO</a></li>
      <li><a href="blog.php" id="blog">BLOG</a></li>
      <li><a href="login.php" class="activo" id="private">AREA PRIVADA</a></li>
    </ul>
  </div>
</div>
<div class="row">
	<div class="col-md-12 col-xs-12 col-sm-12" id="main">
    <div class="container" id="formulario">

      <form class="form-signin" action="" method="POST" id="login">

        <h2 class="form-signin-heading">Por favor, inicie sesión.</h2>
        <div id="error" class="alert alert-danger err" role="alert"></div>
        <label for="usuario">Usuario</label>
        <input type="text" id="usuario" class="form-control" placeholder="Nombre se usuario" name="usuario" required>
        <label for="password" >Contraseña</label>
        <input type="password" id="password" class="form-control" placeholder="Contraseña" name="password" required>
        <button class="btn btn-lg btn-primary btn-block" type="button" id="log">Log In</button>

      </form>

    </div>		
	</div>
</div>
<div class="row">
  <div class="col-md-5"></div>
  <div class="col-md-2 col-xs-12 col-sm-12" id="footer-social">
    <div class="container"><?php include("footer.html");?></div>
  </div>
  <div class="col-md-5"></div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript">
    $("#log").click(function(){
      var user = $("#usuario").val();
      var pass = $("#password").val();
      if(user == "" || pass == ""){//Si estan ambos vacios
        $("#error").empty();
        $("#error").append("<p>Uno de los campos está vacío</p>");
        $("#error").removeClass("err").css("display","block").fadeOut(6000);
      }else{//Si estan ambos llenos
      $.getJSON( "php/obtener_usuario.php",{us:user,pa:pass})
      .done(function(json){
        numero_usuarios = json.length;
        for (i=0 ; i < numero_usuarios; i++){
            usuarios = json[i].user;
            passes = json[i].pass;
              if(usuarios == user){
                if(passes == pass){
                  $("#login").submit();
                }else{
                  //Contraseña incorrecta
                          $("#error").empty();
                          $("#error").append("<p>La contraseña es incorrecta</p>");
                          $("#error").removeClass("err").css("display","block").fadeOut(6000);
                }
              }else{
                //Usuario incorrecto
                        $("#error").empty();
                        $("#error").append("<p>El usuario no ha sido registrado</p>");
                        $("#error").removeClass("err").css("display","block").fadeOut(6000);
              }
        }
      });
    }

    })
</script>
</body>
</html>