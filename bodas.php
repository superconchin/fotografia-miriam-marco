<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <title>Miriam Marco, estudio de fotografía</title>
    <meta name="description" content="estudio de fotografía, en Teruel">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <style type="text/css">
    	#text p{
    		font-size: 18px;
    	}
.btn_contact{
	cursor: pointer;
	cursor: hand;
}
    </style>
</head>
<body>
<div class="row">
	<div class="col-md-12" id="logo">
			<img src="imagenes/logo.jpg" class="center-block">	
	</div>
</div>
<div class="row">
	<div class="col-md-12" id="menu">
		<ul>
			<li><a href="index.php"  id="home">HOME</a></li>
			<li><a href="bodas.php" class="activo" id="bodas">BODAS</a></li>
			<li><a href="contacto.php" id="contacto">CONTACTO</a></li>
			<li><a href="blog.php" id="blog">BLOG</a></li>
			<li><a href="login.php" id="private">AREA PRIVADA</a></li>
		</ul>
	</div>
</div>
<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6 col-xs-12 col-sm-12" id="video">
		<div class="embed-responsive embed-responsive-16by9">
		<video src="imagenes/video.mp4" autoplay></video>
		</div>
	</div>
	<div class="col-md-3"></div>
</div>
<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6 col-xs-12 col-sm-12" id="imagen_home">
		<img src="imagenes/imagen_home.png" class="img-responsive">
	</div>
	<div class="col-md-3"></div>
</div>
<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6 col-sm-12 col-xs-12" id="text">
		<p >¡Hola! Soy Miriam Marco, fotógrafa de bodas en Teruel, en Valencia y en donde tú quieras, dentro de España o más allá, no hay límites. Sé que soy capaz de hacer algo bello, que emociona a quien lo ve despueés, y sin duda la parte que más me gusta de este trabajo.</p>
		<p>Aunque a decir verdad la fotografía no es para mí solo un trabajo, es mucho más, es mi pasión. Crear recuerdos, contar vuestro ía con imagenes que transmiten emociones, risa, llanto, alegrías,nervios... cuidando la técnica, la luz los detalles, para crear imágene bellas y creativas, ese es mi principal objetivo y lo que me hace feliz.</p>
		<p>Enhorabuena por ese bonito paso que vais a dar en vuestras vidas. Cada boda es única e irrepetible, se prepara con mucha ilusión durante mucho tiempo y es importante cada detalle, cada invitado... tenéis que aseguraros de tener un gran reportaje, cuyas imagenes os recuerden todo tal y como fue. Pensad que en el futuro es lo que verán vuestros hijos y nietos y para ellos tendrá un valor incalculable.</p>
		<p>Considero que el fotógrafo tiene que ser como un invitado más, y así es como actúo yo. Es importante pasar desapercibido para captar esos gestos y miradas, vivir intensamente el gran día con vosotros y con vuestros invitados, sin interferir, pero justo al lado, capturando lo que veis y lo que no veis y en ningún momento os pediré que poseis, principal característica del fotoperiodismo y de la fotografía documental de boda, con las cuales me identifico.</p>
		<p>Si os casáis próximamente, os han gustado las imágenes y mi forma de ver las bodas, no dudéis en poneros en contacto conmigo, os daré mucha más información acerca de cómo trabajo durante el gran día, sobre la preboda, la postboda, el albúm, el precio... TODO. Solo puedo atender una boda al día, no lo dejeis pasar y contadme como estais planeando vuestro gran día, estare encantada de conocer y narrar vuestra historia.</p>
	</div>
	<div class="col-md-3"></div>
</div>
<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6 col-xs-12 col-sm-12" id="imagen_contact">
		<img src="imagenes/boton_contactar.jpg" class="img-responsive btn_contact">
	</div>
	<div class="col-md-3"></div>
</div>
<div class="row">
	<div class="col-md-5"></div>
	<div class="col-md-2 col-xs-12 col-sm-12" id="footer-social">
		<div class="container"><?php include("footer.html");?></div>
	</div>
	<div class="col-md-5"></div>
</div>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript">
	$("#menu a").click(function(){
		var id = $(this).attr("id");
		$(".activo").removeClass("activo");
		$("#" + id).addClass("activo");
	})
	$(".btn_contact").click(function(){
		window.location.href="contacto.php";
	})
</script>
</body>
</html>